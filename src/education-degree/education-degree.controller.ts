import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { EducationDegreeService } from './education-degree.service';

@Controller('education-degree')
export class EducationDegreeController {
  constructor(
    private readonly educationDegreeService: EducationDegreeService,
  ) {}

  @Get()
  async get() {
    return await this.educationDegreeService.get();
  }

  @Get('/:educationDegreeId')
  async getById(@Param('educationDegreeId') educationDegreeId: string) {
    return await this.educationDegreeService.getById(educationDegreeId);
  }

  @Post()
  async post(@Body() body) {
    return await this.educationDegreeService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.educationDegreeService.delete(id);
  }
}
