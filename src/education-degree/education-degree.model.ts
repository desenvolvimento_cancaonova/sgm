import * as moongose from 'mongoose';

export const EducationDegreeSchema = new moongose.Schema({
  educationDegree: { type: String, required: true },
});

export interface EducationDegree {
  educationDegree: String;
}
