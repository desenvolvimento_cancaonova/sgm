import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EducationDegree } from './education-degree.model';

@Injectable()
export class EducationDegreeService {
  constructor(
    @InjectModel('EducationDegree')
    private readonly educationDegree: Model<EducationDegree>,
  ) {}

  async get() {
    return await this.educationDegree.find().exec();
  }

  async getById(educationDegreeId: string) {
    let educationDegree;

    try {
      educationDegree = await this.educationDegree.findById(educationDegreeId);
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!educationDegree)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return educationDegree;
  }

  async post(payload) {
    return await new this.educationDegree({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.educationDegree.deleteOne({ _id: id }).exec();
  }
}
