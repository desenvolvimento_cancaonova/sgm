import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EducationDegreeController } from './education-degree.controller';
import { EducationDegreeSchema } from './education-degree.model';
import { EducationDegreeService } from './education-degree.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'EducationDegree', schema: EducationDegreeSchema },
    ]),
  ],
  controllers: [EducationDegreeController],
  providers: [EducationDegreeService],
})
export class EducationDegreeModule {}
