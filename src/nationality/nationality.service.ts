import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Nationality } from './nationality.model';

@Injectable()
export class NationalityService {
  constructor(
    @InjectModel('Nationality')
    private readonly nationalityModel: Model<Nationality>,
  ) {}

  async get() {
    return await this.nationalityModel.find().exec();
  }

  async getById(nationalityId: string) {
    let nationality;

    try {
      nationality = await this.nationalityModel.findById(nationalityId).exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!nationality)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return nationality;
  }

  async post(payload) {
    return await new this.nationalityModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.nationalityModel.deleteOne({ _id: id }).exec();
  }
}
