import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NationalityController } from './nationality.controller';
import { NationalitySchema } from './nationality.model';
import { NationalityService } from './nationality.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Nationality', schema: NationalitySchema },
    ]),
  ],
  controllers: [NationalityController],
  providers: [NationalityService],
})
export class NationalityModule {}
