import * as moongose from 'mongoose';

export const NationalitySchema = new moongose.Schema({
  nationality: { type: String, required: true },
});

export interface Nationality {
  nationality: String;
}
