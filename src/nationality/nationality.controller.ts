import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { NationalityService } from './nationality.service';

@Controller('nationality')
export class NationalityController {
  constructor(private readonly nationalityService: NationalityService) {}

  @Get()
  async get() {
    return await this.nationalityService.get();
  }

  @Get('/:nationalityId')
  async getById(@Param('nationalityId') nationalityId: string) {
    return await this.nationalityService.getById(nationalityId);
  }

  @Post()
  async post(@Body() body) {
    return await this.nationalityService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.nationalityService.delete(id);
  }
}
