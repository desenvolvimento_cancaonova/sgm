import * as moongose from 'mongoose';

export const StatusQuoSchema = new moongose.Schema({
  statusQuo: { type: String, required: true },
});

export interface StatusQuo {
  statusQuo: String;
}
