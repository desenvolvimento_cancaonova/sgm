import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { StatusQuoService } from './status-quo.service';

@Controller('status-quo')
export class StatusQuoController {
  constructor(private readonly statusQuoService: StatusQuoService) {}

  @Get()
  async get() {
    return await this.statusQuoService.get();
  }

  @Get('/:statusQuoId')
  async getById(@Param('statusQuoId') statusQuoId: string) {
    return await this.statusQuoService.getById(statusQuoId);
  }

  @Post()
  async post(@Body() body) {
    return await this.statusQuoService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.statusQuoService.delete(id);
  }
}
