import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { StatusQuo } from './status-quo.model';

@Injectable()
export class StatusQuoService {
  constructor(
    @InjectModel('StatusQuo')
    private readonly statusQuoModel: Model<StatusQuo>,
  ) {}

  async get() {
    return await this.statusQuoModel.find().exec();
  }

  async getById(statusQuoId: string) {
    let statusQuo;

    try {
      statusQuo = await this.statusQuoModel.findById(statusQuoId).exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!statusQuo)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return statusQuo;
  }

  async post(payload) {
    return await new this.statusQuoModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.statusQuoModel.deleteOne({ _id: id }).exec();
  }
}
