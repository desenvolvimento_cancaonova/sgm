import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StatusQuoController } from './status-quo.controller';
import { StatusQuoSchema } from './status-quo.model';
import { StatusQuoService } from './status-quo.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'StatusQuo', schema: StatusQuoSchema }]),
  ],
  controllers: [StatusQuoController],
  providers: [StatusQuoService],
})
export class StatusQuoModule {}
