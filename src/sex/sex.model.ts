import * as moongose from 'mongoose';

export const SexSchema = new moongose.Schema({
  sex: { type: String, required: true },
});

export interface Sex {
  sex: String;
}
