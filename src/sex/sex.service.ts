import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Sex } from './sex.model';

@Injectable()
export class SexService {
  constructor(
    @InjectModel('Sex')
    private readonly sexModel: Model<Sex>,
  ) {}

  async get() {
    return await this.sexModel.find().exec();
  }

  async getById(sexId: string) {
    let sex;

    try {
      sex = await this.sexModel.findById(sexId).exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!sex) throw new NotFoundException('Não foi possível encontrar o ID.');

    return sex;
  }

  async post(payload) {
    return await new this.sexModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.sexModel.deleteOne({ _id: id }).exec();
  }
}
