import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SexController } from './sex.controller';
import { SexSchema } from './sex.model';
import { SexService } from './sex.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Sex', schema: SexSchema }])],
  controllers: [SexController],
  providers: [SexService],
})
export class SexModule {}
