import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { SexService } from './sex.service';

@Controller('sex')
export class SexController {
  constructor(private readonly sexService: SexService) {}

  @Get()
  async get() {
    return await this.sexService.get();
  }

  @Get('/:sexId')
  async getById(@Param('sexId') sexId: string) {
    return await this.sexService.getById(sexId);
  }

  @Post()
  async post(@Body() body) {
    return await this.sexService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.sexService.delete(id);
  }
}
