import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StateCommunityController } from './state-community.controller';
import { StateCommunitySchema } from './state-community.model';
import { StateCommunityService } from './state-community.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'StateCommunity', schema: StateCommunitySchema },
    ]),
  ],
  controllers: [StateCommunityController],
  providers: [StateCommunityService],
})
export class StateCommunityModule {}
