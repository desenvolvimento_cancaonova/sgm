import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { StateCommunity } from './state-community.model';

@Injectable()
export class StateCommunityService {
  constructor(
    @InjectModel('StateCommunity')
    private readonly stateCommunityModel: Model<StateCommunity>,
  ) {}

  async get() {
    return await this.stateCommunityModel.find().exec();
  }

  async getById(stateCommunityId: string) {
    let stateCommunity;

    try {
      stateCommunity = await this.stateCommunityModel
        .findById(stateCommunityId)
        .exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!stateCommunity)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return stateCommunity;
  }

  async post(payload) {
    return await new this.stateCommunityModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.stateCommunityModel.deleteOne({ _id: id }).exec();
  }
}
