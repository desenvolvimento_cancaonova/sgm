import * as moongose from 'mongoose';

export const StateCommunitySchema = new moongose.Schema({
  stateCommunity: { type: String, required: true },
});

export interface StateCommunity {
  stateCommunity: String;
}
