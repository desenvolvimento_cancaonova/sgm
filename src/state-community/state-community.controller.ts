import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { StateCommunityService } from './state-community.service';

@Controller('state-community')
export class StateCommunityController {
  constructor(private readonly stateCommunityService: StateCommunityService) {}

  @Get()
  async get() {
    return await this.stateCommunityService.get();
  }

  @Get('/:stateCommunityId')
  async getById(@Param('stateCommunityId') stateCommunityId: string) {
    return await this.stateCommunityService.getById(stateCommunityId);
  }

  @Post()
  async post(@Body() body) {
    return await this.stateCommunityService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.stateCommunityService.delete(id);
  }
}
