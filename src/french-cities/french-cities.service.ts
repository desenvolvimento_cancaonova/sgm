import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FrenchCities } from './french-cities.model';

@Injectable()
export class FrenchCitiesService {
  constructor(
    @InjectModel('FrenchCities')
    private readonly frenchCitiesModel: Model<FrenchCities>,
  ) {}

  async get() {
    return await this.frenchCitiesModel.find().exec();
  }

  async getById(frenchCitiesId: string) {
    let frenchCities;

    try {
      frenchCities = await this.frenchCitiesModel
        .findById(frenchCitiesId)
        .exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!frenchCities)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return frenchCities;
  }

  async post(payload) {
    return await new this.frenchCitiesModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.frenchCitiesModel.deleteOne({ _id: id }).exec();
  }
}
