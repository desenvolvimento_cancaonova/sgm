import * as moongose from 'mongoose';

export const FrenchCitiesSchema = new moongose.Schema({
  city: { type: String, required: true },
});

export interface FrenchCities {
  city: String;
}
