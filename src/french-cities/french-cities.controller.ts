import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { FrenchCitiesService } from './french-cities.service';

@Controller('french-cities')
export class FrenchCitiesController {
  constructor(private readonly frenchCitiesService: FrenchCitiesService) {}

  @Get()
  async get() {
    return await this.frenchCitiesService.get();
  }

  @Get('/:frenchCitiesId')
  async getById(@Param('frenchCitiesId') frenchCitiesId: string) {
    return await this.frenchCitiesService.getById(frenchCitiesId);
  }

  @Post()
  async post(@Body() body) {
    return await this.frenchCitiesService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.frenchCitiesService.delete(id);
  }
}
