import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FrenchCitiesController } from './french-cities.controller';
import { FrenchCitiesSchema } from './french-cities.model';
import { FrenchCitiesService } from './french-cities.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'FrenchCities', schema: FrenchCitiesSchema },
    ]),
  ],
  controllers: [FrenchCitiesController],
  providers: [FrenchCitiesService],
})
export class FrenchCitiesModule {}
