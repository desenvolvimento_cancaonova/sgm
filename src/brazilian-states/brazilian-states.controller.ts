import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { BrazilianStatesService } from './brazilian-states.service';

@Controller('brazilian-states')
export class BrazilianStatesController {
  constructor(
    private readonly brazilianStatesService: BrazilianStatesService,
  ) {}

  @Get()
  async get() {
    return await this.brazilianStatesService.get();
  }

  @Get('/:brazilianStatesId')
  async getById(@Param('brazilianStatesId') brazilianStatesId: string) {
    return await this.brazilianStatesService.getById(brazilianStatesId);
  }

  @Post()
  async post(@Body() body) {
    return await this.brazilianStatesService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.brazilianStatesService.delete(id);
  }
}
