import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BrazilianStates } from './brazilian-states.model';

@Injectable()
export class BrazilianStatesService {
  constructor(
    @InjectModel('BrazilianStates')
    private readonly brazilianStatesModel: Model<BrazilianStates>,
  ) {}

  async get() {
    return await this.brazilianStatesModel.find().exec();
  }

  async getById(brazilianStatesId: string) {
    let brazilianStates;

    try {
      brazilianStates = await this.brazilianStatesModel
        .findById(brazilianStatesId)
        .exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!brazilianStates)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return brazilianStates;
  }

  async post(payload) {
    return await new this.brazilianStatesModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.brazilianStatesModel.deleteOne({ _id: id }).exec();
  }
}
