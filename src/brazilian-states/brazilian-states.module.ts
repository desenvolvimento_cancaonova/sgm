import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BrazilianStatesController } from './brazilian-states.controller';
import { BrazilianStatesSchema } from './brazilian-states.model';
import { BrazilianStatesService } from './brazilian-states.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'BrazilianStates', schema: BrazilianStatesSchema },
    ]),
  ],
  controllers: [BrazilianStatesController],
  providers: [BrazilianStatesService],
})
export class BrazilianStatesModule {}
