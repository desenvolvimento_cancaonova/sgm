import * as moongose from 'mongoose';

export const BrazilianStatesSchema = new moongose.Schema({
  state: { type: String, required: true },
});

export interface BrazilianStates {
  state: String;
}
