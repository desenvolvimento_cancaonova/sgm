import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MaritalStatusController } from './marital-status.controller';
import { MaritalStatusSchema } from './marital-status.model';
import { MaritalStatusService } from './marital-status.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'MaritalStatus', schema: MaritalStatusSchema },
    ]),
  ],
  controllers: [MaritalStatusController],
  providers: [MaritalStatusService],
})
export class MaritalStatusModule {}
