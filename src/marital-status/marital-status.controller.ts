import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { MaritalStatusService } from './marital-status.service';

@Controller('marital-status')
export class MaritalStatusController {
  constructor(private readonly maritalStatusService: MaritalStatusService) {}

  @Get()
  async get() {
    return await this.maritalStatusService.get();
  }

  @Get('/:maritalStatusId')
  async getById(@Param('maritalStatusId') maritalStatusId: string) {
    return await this.maritalStatusService.getById(maritalStatusId);
  }

  @Post()
  async post(@Body() body) {
    return await this.maritalStatusService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.maritalStatusService.delete(id);
  }
}
