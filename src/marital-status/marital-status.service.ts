import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MaritalStatus } from './marital-status.model';

@Injectable()
export class MaritalStatusService {
  constructor(
    @InjectModel('MaritalStatus')
    private readonly maritalStatusModel: Model<MaritalStatus>,
  ) {}

  async get() {
    return await this.maritalStatusModel.find().exec();
  }

  async getById(maritalStatusId: string) {
    let maritalStatus;

    try {
      maritalStatus = await this.maritalStatusModel
        .findById(maritalStatusId)
        .exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!maritalStatus)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return maritalStatus;
  }

  async post(payload) {
    return await new this.maritalStatusModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.maritalStatusModel.deleteOne({ _id: id }).exec();
  }
}
