import * as moongose from 'mongoose';

export const MaritalStatusSchema = new moongose.Schema({
  maritalStatus: { type: String, required: true },
});

export interface MaritalStatus {
  maritalStatus: String;
}
