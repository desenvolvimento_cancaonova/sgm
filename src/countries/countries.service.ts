import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Countries } from './countries.model';

@Injectable()
export class CountriesService {
  constructor(
    @InjectModel('Countries')
    private readonly countriesModel: Model<Countries>,
  ) {}

  async get() {
    return await this.countriesModel.find().exec();
  }

  async getById(countriesId: string) {
    let countries;

    try {
      countries = await this.countriesModel.findById(countriesId).exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!countries)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return countries;
  }

  async post(payload) {
    return await new this.countriesModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.countriesModel.deleteOne({ _id: id }).exec();
  }
}
