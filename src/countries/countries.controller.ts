import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { CountriesService } from './countries.service';

@Controller('countries')
export class CountriesController {
  constructor(private readonly countriesService: CountriesService) {}

  @Get()
  async get() {
    return await this.countriesService.get();
  }

  @Get('/:countriesId')
  async getById(@Param('countriesId') countriesId: string) {
    return await this.countriesService.getById(countriesId);
  }

  @Post()
  async postCountries(@Body() body) {
    return await this.countriesService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.countriesService.delete(id);
  }
}
