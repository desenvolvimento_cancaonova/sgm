import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CountriesController } from './countries.controller';
import { CountriesSchema } from './countries.model';
import { CountriesService } from './countries.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Countries', schema: CountriesSchema }]),
  ],
  controllers: [CountriesController],
  providers: [CountriesService],
})
export class CountriesModule {}
