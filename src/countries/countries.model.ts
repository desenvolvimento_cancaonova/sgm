import * as moongose from 'mongoose';

export const CountriesSchema = new moongose.Schema({
  country: { type: String, required: true },
});

export interface Countries {
  country: String;
}
