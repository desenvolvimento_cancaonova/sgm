import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { CommitmentModeService } from './commitment-mode.service';

@Controller('commitment-mode')
export class CommitmentModeController {
  constructor(private readonly commitmentModeService: CommitmentModeService) {}

  @Get()
  async get() {
    return await this.commitmentModeService.get();
  }

  @Get('/:commitmentId')
  async getById(@Param('commitmentId') commitmentId: string) {
    return await this.commitmentModeService.getById(commitmentId);
  }

  @Post()
  async post(@Body() body) {
    return await this.commitmentModeService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.commitmentModeService.delete(id);
  }
}
