import * as moongose from 'mongoose';

export const CommitmentModeSchema = new moongose.Schema({
  commitment: { type: String, required: true },
});

export interface CommitmentMode {
  commitment: String;
}
