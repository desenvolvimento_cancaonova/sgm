import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CommitmentModeController } from './commitment-mode.controller';
import { CommitmentModeSchema } from './commitment-mode.model';
import { CommitmentModeService } from './commitment-mode.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'CommitmentMode', schema: CommitmentModeSchema },
    ]),
  ],
  controllers: [CommitmentModeController],
  providers: [CommitmentModeService],
})
export class CommitmentModeModule {}
