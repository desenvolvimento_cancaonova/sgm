import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CommitmentMode } from './commitment-mode.model';

@Injectable()
export class CommitmentModeService {
  constructor(
    @InjectModel('CommitmentMode')
    private readonly commitmentModeModel: Model<CommitmentMode>,
  ) {}

  async get() {
    return await this.commitmentModeModel.find().exec();
  }

  async getById(commitmentId: string) {
    let commitment;

    try {
      commitment = await this.commitmentModeModel.findById(commitmentId).exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!commitment)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return commitment;
  }

  async post(payload) {
    return await new this.commitmentModeModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.commitmentModeModel.deleteOne({ _id: id }).exec();
  }
}
