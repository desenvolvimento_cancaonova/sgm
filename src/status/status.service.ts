import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Status } from './status.model';

@Injectable()
export class StatusService {
  constructor(
    @InjectModel('Status')
    private readonly statusModel: Model<Status>,
  ) {}

  async get() {
    return await this.statusModel.find().exec();
  }

  async getById(statusId: string) {
    let status;

    try {
      status = await this.statusModel.findById(statusId).exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!status)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return status;
  }

  async post(payload) {
    return await new this.statusModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.statusModel.deleteOne({ _id: id }).exec();
  }
}
