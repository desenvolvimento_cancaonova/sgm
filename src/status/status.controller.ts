import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { StatusService } from './status.service';

@Controller('status')
export class StatusController {
  constructor(private readonly statusService: StatusService) {}

  @Get()
  async get() {
    return await this.statusService.get();
  }

  @Get('/:statusId')
  async getById(@Param('statusId') statusId: string) {
    return await this.statusService.getById(statusId);
  }

  @Post()
  async post(@Body() body) {
    return await this.statusService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.statusService.delete(id);
  }
}
