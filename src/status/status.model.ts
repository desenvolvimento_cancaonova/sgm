import * as moongose from 'mongoose';

export const StatusSchema = new moongose.Schema({
  status: { type: String, required: true },
});

export interface Status {
  status: String;
}
