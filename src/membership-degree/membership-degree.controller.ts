import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { MembershipDegreeService } from './membership-degree.service';

@Controller('membership-degree')
export class MembershipDegreeController {
  constructor(
    private readonly membershipDegreeService: MembershipDegreeService,
  ) {}

  @Get()
  async get() {
    return await this.membershipDegreeService.get();
  }

  @Get('/:membershipDegreeId')
  async getById(@Param('membershipDegreeId') membershipDegreeId: string) {
    return await this.membershipDegreeService.getById(membershipDegreeId);
  }

  @Post()
  async post(@Body() body) {
    return await this.membershipDegreeService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.membershipDegreeService.delete(id);
  }
}
