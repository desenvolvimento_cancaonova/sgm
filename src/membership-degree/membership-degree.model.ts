import * as moongose from 'mongoose';

export const MembershipDegreeSchema = new moongose.Schema({
  membershipDegree: { type: String, required: true },
});

export interface MembershipDegree {
  membershipDegree: String;
}
