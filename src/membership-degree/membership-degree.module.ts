import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MembershipDegreeController } from './membership-degree.controller';
import { MembershipDegreeSchema } from './membership-degree.model';
import { MembershipDegreeService } from './membership-degree.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'MembershipDegree', schema: MembershipDegreeSchema },
    ]),
  ],
  controllers: [MembershipDegreeController],
  providers: [MembershipDegreeService],
})
export class MembershipDegreeModule {}
