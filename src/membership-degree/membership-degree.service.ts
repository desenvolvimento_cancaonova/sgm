import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MembershipDegree } from './membership-degree.model';

@Injectable()
export class MembershipDegreeService {
  constructor(
    @InjectModel('MembershipDegree')
    private readonly membershipDegreeModel: Model<MembershipDegree>,
  ) {}

  async get() {
    return await this.membershipDegreeModel.find().exec();
  }

  async getById(membershipDegreeId: string) {
    let membershipDegree;

    try {
      membershipDegree = await this.membershipDegreeModel
        .findById(membershipDegreeId)
        .exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!membershipDegree)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return membershipDegree;
  }

  async post(payload) {
    return await new this.membershipDegreeModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.membershipDegreeModel.deleteOne({ _id: id }).exec();
  }
}
