import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Profession } from './profession.model';

@Injectable()
export class ProfessionService {
  constructor(
    @InjectModel('Profession')
    private readonly professionModel: Model<Profession>,
  ) {}

  async get() {
    return await this.professionModel.find().exec();
  }

  async getById(professionId: string) {
    let profession;

    try {
      profession = await this.professionModel.findById(professionId).exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!profession)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return profession;
  }

  async post(payload) {
    return await new this.professionModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.professionModel.deleteOne({ _id: id }).exec();
  }
}
