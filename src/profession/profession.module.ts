import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProfessionController } from './profession.controller';
import { ProfessionSchema } from './profession.model';
import { ProfessionService } from './profession.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Profession', schema: ProfessionSchema },
    ]),
  ],
  controllers: [ProfessionController],
  providers: [ProfessionService],
})
export class ProfessionModule {}
