import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ProfessionService } from './profession.service';

@Controller('profession')
export class ProfessionController {
  constructor(private readonly professionService: ProfessionService) {}

  @Get()
  async get() {
    return await this.professionService.get();
  }

  @Get('/:professionId')
  async getById(@Param('professionId') professionId: string) {
    return await this.professionService.getById(professionId);
  }

  @Post()
  async post(@Body() body) {
    return await this.professionService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.professionService.delete(id);
  }
}
