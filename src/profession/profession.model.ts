import * as moongose from 'mongoose';

export const ProfessionSchema = new moongose.Schema({
  profession: { type: String, required: true },
});

export interface Profession {
  profession: String;
}
