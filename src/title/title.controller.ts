import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { TitleService } from './title.service';

@Controller('title')
export class TitleController {
  constructor(private readonly titleService: TitleService) {}

  @Get()
  async get() {
    return await this.titleService.get();
  }

  @Get('/:titleId')
  async getById(@Param('titleId') titleId: string) {
    return await this.titleService.getById(titleId);
  }

  @Post()
  async post(@Body() body) {
    return await this.titleService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.titleService.delete(id);
  }
}
