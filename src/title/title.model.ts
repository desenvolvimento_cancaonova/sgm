import * as moongose from 'mongoose';

export const TitleSchema = new moongose.Schema({
  title: { type: String, required: true },
});

export interface Title {
  title: String;
}
