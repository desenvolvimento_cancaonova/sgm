import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Title } from './title.model';

@Injectable()
export class TitleService {
  constructor(
    @InjectModel('Title')
    private readonly titleModel: Model<Title>,
  ) {}

  async get() {
    return await this.titleModel.find().exec();
  }

  async getById(titleId: string) {
    let title;

    try {
      title = await this.titleModel.findById(titleId).exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!title) throw new NotFoundException('Não foi possível encontrar o ID.');

    return title;
  }

  async post(payload) {
    return await new this.titleModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.titleModel.deleteOne({ _id: id }).exec();
  }
}
