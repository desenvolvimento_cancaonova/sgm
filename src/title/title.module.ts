import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TitleController } from './title.controller';
import { TitleSchema } from './title.model';
import { TitleService } from './title.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Title', schema: TitleSchema }]),
  ],
  controllers: [TitleController],
  providers: [TitleService],
})
export class TitleModule {}
