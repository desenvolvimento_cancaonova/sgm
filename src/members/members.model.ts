import * as moongose from 'mongoose';

export const MembersSchema = new moongose.Schema({
  name: { type: String, required: true },
});

export interface Members {
  name: String;
}
