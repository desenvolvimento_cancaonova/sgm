import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { MembersService } from './members.service';

@Controller('members')
export class MembersController {
  constructor(private readonly membersService: MembersService) {}

  @Get()
  async get() {
    return await this.membersService.get();
  }

  @Get('/:memberId')
  async getById(@Param('memberId') memberId: string) {
    return await this.membersService.getById(memberId);
  }

  @Post()
  async post(@Body() body) {
    return await this.membersService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.membersService.delete(id);
  }
}
