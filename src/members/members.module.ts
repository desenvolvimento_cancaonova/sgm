import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MembersController } from './members.controller';
import { MembersSchema } from './members.model';
import { MembersService } from './members.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Members', schema: MembersSchema }]),
  ],
  controllers: [MembersController],
  providers: [MembersService],
})
export class MembersModule {}
