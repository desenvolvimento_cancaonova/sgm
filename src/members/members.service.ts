import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Members } from './members.model';

@Injectable()
export class MembersService {
  constructor(
    @InjectModel('Members')
    private readonly membersModel: Model<Members>,
  ) {}

  async get() {
    return await this.membersModel.find().exec();
  }

  async getById(memberId: string) {
    let member;

    try {
      member = await this.membersModel.findById(memberId).exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!member)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return member;
  }

  async post(payload) {
    return await new this.membersModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.membersModel.deleteOne({ _id: id }).exec();
  }
}
