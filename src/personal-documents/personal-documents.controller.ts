import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { PersonalDocumentsService } from './personal-documents.service';

@Controller('personal-documents')
export class PersonalDocumentsController {
  constructor(
    private readonly personalDocumentsService: PersonalDocumentsService,
  ) {}

  @Get()
  async get() {
    return await this.personalDocumentsService.get();
  }

  @Get('/:personalDocumentsId')
  async getById(@Param('personalDocumentsId') personalDocumentsId: string) {
    return await this.personalDocumentsService.getById(personalDocumentsId);
  }

  @Post()
  async post(@Body() body) {
    return await this.personalDocumentsService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.personalDocumentsService.delete(id);
  }
}
