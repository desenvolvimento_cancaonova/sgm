import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PersonalDocumentsController } from './personal-documents.controller';
import { PersonalDocumentsSchema } from './personal-documents.model';
import { PersonalDocumentsService } from './personal-documents.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'PersonalDocuments', schema: PersonalDocumentsSchema },
    ]),
  ],
  controllers: [PersonalDocumentsController],
  providers: [PersonalDocumentsService],
})
export class PersonalDocumentsModule {}
