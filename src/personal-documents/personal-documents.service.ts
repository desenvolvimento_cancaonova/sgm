import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PersonalDocuments } from './personal-documents.model';

@Injectable()
export class PersonalDocumentsService {
  constructor(
    @InjectModel('PersonalDocuments')
    private readonly personalDocumentsModel: Model<PersonalDocuments>,
  ) {}

  async get() {
    return await this.personalDocumentsModel.find().exec();
  }

  async getById(personalDocumentsId: string) {
    let personalDocuments;

    try {
      personalDocuments = await this.personalDocumentsModel
        .findById(personalDocumentsId)
        .exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!personalDocuments)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return personalDocuments;
  }

  async post(payload) {
    return await new this.personalDocumentsModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.personalDocumentsModel.deleteOne({ _id: id }).exec();
  }
}
