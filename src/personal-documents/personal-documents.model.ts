import * as moongose from 'mongoose';

export const PersonalDocumentsSchema = new moongose.Schema({
  document: { type: String, required: true },
});

export interface PersonalDocuments {
  document: String;
}
