import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { AgeGroupService } from './age-group.service';

@Controller('age-group')
export class AgeGroupController {
  constructor(private readonly ageGroupService: AgeGroupService) {}

  @Get()
  async get() {
    return await this.ageGroupService.get();
  }

  @Get('/:ageGroupId')
  async getById(@Param('ageGroupId') ageGroupId: string) {
    return await this.ageGroupService.getById(ageGroupId);
  }

  @Post()
  async post(@Body() body) {
    return await this.ageGroupService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.ageGroupService.delete(id);
  }
}
