import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AgeGroup } from './age-group.model';

@Injectable()
export class AgeGroupService {
  constructor(
    @InjectModel('AgeGroup')
    private readonly ageGroupModel: Model<AgeGroup>,
  ) {}

  async get() {
    return await this.ageGroupModel.find().exec();
  }

  async getById(ageGroupId: string) {
    let ageGroup;

    try {
      ageGroup = await this.ageGroupModel.findById(ageGroupId).exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!ageGroup)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return ageGroup;
  }

  async post(payload) {
    return await new this.ageGroupModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.ageGroupModel.deleteOne({ _id: id }).exec();
  }
}
