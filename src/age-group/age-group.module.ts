import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AgeGroupController } from './age-group.controller';
import { AgeGroupSchema } from './age-group.model';
import { AgeGroupService } from './age-group.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'AgeGroup', schema: AgeGroupSchema }]),
  ],
  controllers: [AgeGroupController],
  providers: [AgeGroupService],
})
export class AgeGroupModule {}
