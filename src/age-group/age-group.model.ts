import * as moongose from 'mongoose';

export const AgeGroupSchema = new moongose.Schema({
  ageGroup: { type: String, required: true },
});

export interface AgeGroup {
  ageGroup: String;
}
