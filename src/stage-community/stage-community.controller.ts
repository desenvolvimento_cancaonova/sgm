import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { StageCommunityService } from './stage-community.service';

@Controller('stage-community')
export class StageCommunityController {
  constructor(private readonly stageCommunityService: StageCommunityService) {}

  @Get()
  async get() {
    return await this.stageCommunityService.get();
  }

  @Get('/:stageCommunityId')
  async getById(@Param('stageCommunityId') stageCommunityId: string) {
    return await this.stageCommunityService.getById(stageCommunityId);
  }

  @Post()
  async post(@Body() body) {
    return await this.stageCommunityService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.stageCommunityService.delete(id);
  }
}
