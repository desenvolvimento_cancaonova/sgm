import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StageCommunityController } from './stage-community.controller';
import { StageCommunitySchema } from './stage-community.model';
import { StageCommunityService } from './stage-community.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'StageCommunity', schema: StageCommunitySchema },
    ]),
  ],
  controllers: [StageCommunityController],
  providers: [StageCommunityService],
})
export class StageCommunityModule {}
