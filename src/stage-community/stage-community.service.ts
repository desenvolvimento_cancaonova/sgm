import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { StageCommunity } from './stage-community.model';

@Injectable()
export class StageCommunityService {
  constructor(
    @InjectModel('StageCommunity')
    private readonly stageCommunityModel: Model<StageCommunity>,
  ) {}

  async get() {
    return await this.stageCommunityModel.find().exec();
  }

  async getById(stageCommunityId: string) {
    let stageCommunity;

    try {
      stageCommunity = await this.stageCommunityModel
        .findById(stageCommunityId)
        .exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!stageCommunity)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return stageCommunity;
  }

  async post(payload) {
    return await new this.stageCommunityModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.stageCommunityModel.deleteOne({ _id: id }).exec();
  }
}
