import * as moongose from 'mongoose';

export const StageCommunitySchema = new moongose.Schema({
  stageCommunity: { type: String, required: true },
});

export interface StageCommunity {
  stageCommunity: String;
}
