import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { CanonStatusService } from './canon-status.service';

@Controller('canon-status')
export class CanonStatusController {
  constructor(private readonly canonStatusService: CanonStatusService) {}

  @Get()
  async get() {
    return await this.canonStatusService.get();
  }

  @Get('/:canonStatusId')
  async getById(@Param('canonStatusId') canonStatusId: string) {
    return await this.canonStatusService.getById(canonStatusId);
  }

  @Post()
  async post(@Body() body) {
    return await this.canonStatusService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.canonStatusService.delete(id);
  }
}
