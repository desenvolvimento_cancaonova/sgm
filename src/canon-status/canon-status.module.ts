import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CanonStatusController } from './canon-status.controller';
import { CanonStatusSchema } from './canon-status.model';
import { CanonStatusService } from './canon-status.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'CanonStatus', schema: CanonStatusSchema },
    ]),
  ],
  controllers: [CanonStatusController],
  providers: [CanonStatusService],
})
export class CanonStatusModule {}
