import * as moongose from 'mongoose';

export const CanonStatusSchema = new moongose.Schema({
  canonStatus: { type: String, required: true },
});

export interface CanonStatus {
  canonStatus: String;
}
