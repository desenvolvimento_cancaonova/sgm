import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CanonStatus } from './canon-status.model';

@Injectable()
export class CanonStatusService {
  constructor(
    @InjectModel('CanonStatus')
    private readonly canonStatusModel: Model<CanonStatus>,
  ) {}

  async get() {
    return await this.canonStatusModel.find().exec();
  }

  async getById(canonStatusId: string) {
    let canonStatus;

    try {
      canonStatus = await this.canonStatusModel.findById(canonStatusId).exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!canonStatus)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return canonStatus;
  }

  async post(payload) {
    return await new this.canonStatusModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.canonStatusModel.deleteOne({ _id: id }).exec();
  }
}
