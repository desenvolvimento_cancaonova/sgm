import * as moongose from 'mongoose';

export const MembershipSchema = new moongose.Schema({
  membership: { type: String, required: true },
});

export interface Membership {
  membership: String;
}
