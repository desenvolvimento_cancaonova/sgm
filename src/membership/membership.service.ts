import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Membership } from './membership.model';

@Injectable()
export class MembershipService {
  constructor(
    @InjectModel('Membership')
    private readonly membershipModel: Model<Membership>,
  ) {}

  async get() {
    return await this.membershipModel.find().exec();
  }

  async getById(membershipId: string) {
    let membership;

    try {
      membership = await this.membershipModel.findById(membershipId).exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!membership)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return membership;
  }

  async post(payload) {
    return await new this.membershipModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.membershipModel.deleteOne({ _id: id }).exec();
  }
}
