import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { MembershipService } from './membership.service';

@Controller('membership')
export class MembershipController {
  constructor(private readonly membershipService: MembershipService) {}

  @Get()
  async get() {
    return await this.membershipService.get();
  }

  @Get('/:membershipId')
  async getById(@Param('membershipId') membershipId: string) {
    return await this.membershipService.getById(membershipId);
  }

  @Post()
  async post(@Body() body) {
    return await this.membershipService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.membershipService.delete(id);
  }
}
