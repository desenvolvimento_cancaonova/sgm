import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BrazilianCitiesController } from './brazilian-cities.controller';
import { BrazilianCitiesSchema } from './brazilian-cities.model';
import { BrazilianCitiesService } from './brazilian-cities.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'BrazilianCities', schema: BrazilianCitiesSchema },
    ]),
  ],
  controllers: [BrazilianCitiesController],
  providers: [BrazilianCitiesService],
})
export class BrazilianCitiesModule {}
