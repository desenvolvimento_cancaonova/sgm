import * as moongose from 'mongoose';

export const BrazilianCitiesSchema = new moongose.Schema({
  city: { type: String, required: true },
});

export interface BrazilianCities {
  city: String;
}
