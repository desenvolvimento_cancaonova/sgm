import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { BrazilianCitiesService } from './brazilian-cities.service';

@Controller('brazilian-cities')
export class BrazilianCitiesController {
  constructor(
    private readonly brazilianCitiesService: BrazilianCitiesService,
  ) {}

  @Get()
  async get() {
    return await this.brazilianCitiesService.get();
  }

  @Get('/:brazilianCitiesId')
  async getById(@Param('brazilianCitiesId') brazilianCitiesId: string) {
    return await this.brazilianCitiesService.getById(brazilianCitiesId);
  }

  @Post()
  async post(@Body() body) {
    return await this.brazilianCitiesService.post(body);
  }

  @Delete(':id')
  removeProduct(@Param('id') id: string) {
    this.brazilianCitiesService.delete(id);
  }
}
