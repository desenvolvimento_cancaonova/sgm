import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BrazilianCities } from './brazilian-cities.model';

@Injectable()
export class BrazilianCitiesService {
  constructor(
    @InjectModel('BrazilianCities')
    private readonly brazilianCitiesModel: Model<BrazilianCities>,
  ) {}

  async get() {
    return await this.brazilianCitiesModel.find().exec();
  }

  async getById(brazilianCitiesId: string) {
    let brazilianCities;

    try {
      brazilianCities = await this.brazilianCitiesModel
        .findById(brazilianCitiesId)
        .exec();
    } catch {
      throw new NotFoundException('ID inválido!');
    }

    if (!brazilianCities)
      throw new NotFoundException('Não foi possível encontrar o ID.');

    return brazilianCities;
  }

  async post(payload) {
    return await new this.brazilianCitiesModel({
      ...payload,
    }).save();
  }

  delete(id: string) {
    this.brazilianCitiesModel.deleteOne({ _id: id }).exec();
  }
}
