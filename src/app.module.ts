import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AgeGroupModule } from './age-group/age-group.module';
import { BrazilianCitiesModule } from './brazilian-cities/brazilian-cities.module';
import { BrazilianStatesModule } from './brazilian-states/brazilian-states.module';
import { CanonStatusModule } from './canon-status/canon-status.module';
import { CommitmentModeModule } from './commitment-mode/commitment-mode.module';
import { CountriesModule } from './countries/countries.module';
import { EducationDegreeModule } from './education-degree/education-degree.module';
import { FrenchCitiesModule } from './french-cities/french-cities.module';
import { MaritalStatusModule } from './marital-status/marital-status.module';
import { MembersModule } from './members/members.module';
import { MembershipDegreeModule } from './membership-degree/membership-degree.module';
import { MembershipModule } from './membership/membership.module';
import { NationalityModule } from './nationality/nationality.module';
import { PersonalDocumentsModule } from './personal-documents/personal-documents.module';
import { ProfessionModule } from './profession/profession.module';
import { SexModule } from './sex/sex.module';
import { StageCommunityModule } from './stage-community/stage-community.module';
import { StateCommunityModule } from './state-community/state-community.module';
import { StatusQuoModule } from './status-quo/status-quo.module';
import { StatusModule } from './status/status.module';
import { TitleModule } from './title/title.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      'mongodb+srv://desenvolvimento:%23q1w2e3%23@cluster0.08e3m.mongodb.net/sgcm?retryWrites=true&w=majority',
    ),
    AgeGroupModule,
    BrazilianCitiesModule,
    BrazilianStatesModule,
    CanonStatusModule,
    CommitmentModeModule,
    CountriesModule,
    EducationDegreeModule,
    FrenchCitiesModule,
    MaritalStatusModule,
    MembersModule,
    MembershipModule,
    MembershipDegreeModule,
    NationalityModule,
    PersonalDocumentsModule,
    ProfessionModule,
    SexModule,
    StageCommunityModule,
    StateCommunityModule,
    StatusModule,
    StatusQuoModule,
    TitleModule,
  ],
})
export class AppModule {}
